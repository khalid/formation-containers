Nous allons essayer de containeuriser une application qui permet de compter le nombre de séquences dans un fichier fasta :

grep permet de compter l'occurence des '>' en début de chaque identifiant de séquence :

grep -c '>' test.fa

On peut également écrire un script R utilisant le package seqinr pour lire le fichier fasta et compter le nombre de séquences.

Ce script peut être appelé de cette manière :

Rscript --vanilla src/countFasta.R fasta/test.fa


On peut créer un script php pour wrapper l'appel à grep et au script R  :

php src/count_seq.php

L'avantage de ce wrapper est de pouvoir accéder aux résultats via un navigateur web quand ce fichier est dans un dossier servit par un serveur web apache.

Pour que cela fonctionne, il faut les éléments suivants :
* Une distribution linux
* php + apache2
* R
* le package seqinr

Pour être sûr de garantir l'environnement d'exécution de notre script, nous allons construire un container Docker dont les détails sont dans le fichier Dockerfile

Depuis le dossier contenant le fichier Dockefile on construit l'mage en tapant :

docker build -t count-seq-image .

Pour lancer un conteneur :

docker run -p 80:80 count-seq-image

dans votre navigateur ouvrir cette url :

http://127.0.0.1/src/count-seq.php

pour examiner ce qu'il y à l'intérieur du container

docker run -i -t count-seq-image /bin/bash


Ajouter une sequence puis re-consulter l'URL

Commenter le résultat.

Jusqu'ici, le script analysait les données du fichier intégré dans le container, pour travailler sur les données de l'hôte.

Mettre le fichier test.fa dans un dossier fasta ex: /home/nom_utilisateur/fasta/ puis lancer :

docker run -p 80:80 -v /home/nom_utilisateur/fasta/:/var/www/html/fasta count-seq-image

