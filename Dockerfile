FROM php:7.0-apache

#installer R
RUN apt-get update
RUN apt-get install -y r-base r-base-dev

#installer le package seqinr
RUN R --slave -e "install.packages( 'seqinr', repos='https://cloud.r-project.org')"

#Ici on copie les scripts dans le conteneur
COPY src/ /var/www/html/src

#Ici on copie également les données dans le conteneur
COPY fasta /var/www/html/fasta

RUN echo "ServerName 127.0.0.1" >> /etc/apache2/sites-enabled/000-default.conf
EXPOSE 80


